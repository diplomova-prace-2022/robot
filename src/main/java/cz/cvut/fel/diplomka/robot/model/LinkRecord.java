package cz.cvut.fel.diplomka.robot.model;

import java.io.Serializable;

public record LinkRecord(String linkTo, String title, String foundAt) implements Serializable {
}
