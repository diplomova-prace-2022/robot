package cz.cvut.fel.diplomka.robot.service;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class DomainService {

    private static final String DOMAINS_FILE_PATH = "words.txt";
    private final List<String> domains;

    public DomainService() {
        domains = new ArrayList<>();
        try(InputStream is = getClass().getClassLoader().getResourceAsStream(DOMAINS_FILE_PATH)){
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line = reader.readLine()) != null){
                domains.add(line + ".com");
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to open file " + DOMAINS_FILE_PATH);
        }
    }

    public String getRandomDomain(){
        int index = ThreadLocalRandom.current().nextInt(0, domains.size());
        return domains.get(index);
    }

    public List<String> getRandomDomains(int count){
        List<String> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(getRandomDomain());
        }
        return result;
    }

}
