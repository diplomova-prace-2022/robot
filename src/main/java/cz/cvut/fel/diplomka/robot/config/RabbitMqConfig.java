package cz.cvut.fel.diplomka.robot.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    private static final String ROBOT_TASKS_EXCHANGE_NAME = "robots";
    private static final String LINKS_EXCHANGE_NAME = "links";
    private static final String DOMAIN_LINKS_QUEUE_NAME = "find-links-for-domain";

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean(name = "robotExchange")
    public TopicExchange robotExchange(){
        return new TopicExchange(ROBOT_TASKS_EXCHANGE_NAME);
    }

    @Bean(name = "linksExchange")
    public TopicExchange linksExchange(){
        return new TopicExchange(LINKS_EXCHANGE_NAME);
    }

    @Bean(name = "domainLinksQueue")
    public Queue domainLinksQueue(){
        return new Queue(DOMAIN_LINKS_QUEUE_NAME);
    }

}
