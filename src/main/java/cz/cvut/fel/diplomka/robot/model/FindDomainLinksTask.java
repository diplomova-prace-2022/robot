package cz.cvut.fel.diplomka.robot.model;

import java.io.Serializable;

public record FindDomainLinksTask(String domain) implements Serializable {
}
