package cz.cvut.fel.diplomka.robot.service;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class WordService {

    private static final String WORDS_FILE_PATH = "words.txt";
    private final List<String> words;

    public WordService() {
        words = new ArrayList<>();
        try(InputStream is = getClass().getClassLoader().getResourceAsStream(WORDS_FILE_PATH)){
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line = reader.readLine()) != null){
                words.add(line);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to open file " + WORDS_FILE_PATH);
        }
    }

    public String getRandomWord(){
        int index = ThreadLocalRandom.current().nextInt(0, words.size());
        return words.get(index);
    }

    public List<String> getRandomWords(int count){
        List<String> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(getRandomWord());
        }
        return result;
    }

}
