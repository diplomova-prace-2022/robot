package cz.cvut.fel.diplomka.robot.model;

import java.io.Serializable;
import java.util.List;

public record LinksForDomain(String domain, List<LinkRecord> links) implements Serializable {
}
