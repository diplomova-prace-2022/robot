package cz.cvut.fel.diplomka.robot.service;

import cz.cvut.fel.diplomka.robot.model.FindDomainLinksTask;
import cz.cvut.fel.diplomka.robot.model.LinkRecord;
import cz.cvut.fel.diplomka.robot.model.LinksForDomain;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class LinkService {

    private static final long JOB_INTERVAL_HOURS = 12L;

    private final RabbitTemplate rabbitTemplate;
    private final DomainService domainService;
    private final WordService wordService;
    private final TopicExchange linksExchange;

    private final Map<String, LocalDateTime> domainLastJobMap = new ConcurrentHashMap<>();

    @Autowired
    public LinkService(RabbitTemplate rabbitTemplate, DomainService domainService, WordService wordService, @Qualifier("linksExchange") TopicExchange linksExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.domainService = domainService;
        this.wordService = wordService;
        this.linksExchange = linksExchange;
    }

    @RabbitListener(queues = "#{domainLinksQueue.name}")
    public void findDomainLinks(FindDomainLinksTask message) {
        String domain = message.domain();

        if(domainLastJobMap.containsKey(domain)
                && ChronoUnit.HOURS.between(domainLastJobMap.get(domain), LocalDateTime.now()) < JOB_INTERVAL_HOURS) return;
        domainLastJobMap.put(domain, LocalDateTime.now());

        int numOfBatches = ThreadLocalRandom.current().nextInt(5, 10);
        for (int i = 0; i < numOfBatches; i++) {
            int numOfLinks = ThreadLocalRandom.current().nextInt(10, 30);
            List<LinkRecord> links = new ArrayList<>(numOfLinks);
            for (int j = 0; j < numOfLinks; j++) {
                links.add(new LinkRecord(domainService.getRandomDomain(), wordService.getRandomWord(), LocalDateTime.now().toString()));
            }
            rabbitTemplate.convertAndSend(linksExchange.getName(), "links.found", new LinksForDomain(domain, links));
        }
    }

}
