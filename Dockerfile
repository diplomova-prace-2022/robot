FROM openjdk:17-alpine
RUN addgroup -S my_group && adduser -S my_user -G my_group
USER my_user
COPY target/robot*.jar /app/robot.jar
ENTRYPOINT ["java", "-jar", "/app/robot.jar"]
